import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Task1 from '@/views/Task1.vue';
import Task2 from '@/views/Task2.vue';
import Task3 from '@/views/Task3.vue';
import Task4 from '@/views/Task4.vue';
import Order from '@/views/Order.vue';
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/task1',
    name: 'Task1',
    component: Task1,
  },
  {
    path: '/task2',
    name: 'Task2',
    component: Task2,
  },
  {
    path: '/task3',
    name: 'Task3',
    component: Task3,
  },
  {
    path: '/task4',
    name: 'Task4',
    component: Task4,
  },
  {
    path: '/order/:orderId',
    name: 'order',
    component: Order,
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
