import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import maps from './maps.js';
import order from './order.js';
export default new Vuex.Store({
  modules: {
    maps,
    order,
  }
})
