export default {
    namespaced: true,
    state: {
        cities: null,
        streets: null,
        idCityLoaded: null,
        idCitySelected: null,
    },
    getters: {
        getCities(state){
            // возвращает массив городов
            return state.cities;
        },
        getStreets(state){
            // возвращает массив улиц
            return state.streets;
        },
        getActiveCity(state){
            // возвращает совпадение выбранного и загруженного городов
            if(state.idCityLoaded === null){
                return false;
            }
            return state.idCityLoaded === state.idCitySelected;
        },
    },
    mutations: {
        setCities(state, data){
            // записывает массив с данными городов
            state.cities = data;
        },
        setStreets(state, data){
            // записывает массив с данными улиц
            state.streets = data;
        },
        setIdCityLoaded(state, data){
            // записывает id города улицы которого загрузились
            state.idCityLoaded = data;
        },
        setIdCitySelected(state, data){
            // записывает id выбранного в списке города
            state.idCitySelected = data;
        },
        setDefaultValue(state){
            // записывает значения по умолчанию
            state.cities = null;
            state.streets = null;
            state.idCityLoaded = null;
            state.idCitySelected = null;
        },
    },
    actions: {
        putCities({commit}, data){
            // устанавливает загруженный список городов
            commit('setCities', data);
        },
        async putStreets({commit}, data){
            // получает и устанавливает загруженный список улиц по id города
            let responce = await fetch(`http://efit.biz:8080/iikobizdatastore/api/addresses/streets/by-city/${data}`);
            if(responce.ok){
                let result = await responce.json();
                // console.log(result);
                commit('setStreets', result);
                commit('setIdCityLoaded', data);
            }else{
                alert("Ошибка HTTP: " + response.status);
            }
        },
        putIdCitySelected({commit}, data){
            // устанавливает id выбранного города
            commit('setIdCitySelected', data);
        },
        clearStore({commit}){
            // устанавливает дефолтные значения
            commit('setDefaultValue');
        }
    }
}