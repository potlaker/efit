export default {
    namespaced: true,
    state: {
        orderInfo: null,
        togglePopup: false,
    },
    getters: {
        getOrderInfo(state){
            // возвращает информацию о заказе
            return state.orderInfo;
        },
        getStatusPopup(state){
            // возвращает статус popup
            return state.togglePopup;
        },
    },
    mutations: {
        setOrderInfo(state, data){
            // записывает orderInfo - информация о заказе
            state.orderInfo = data;
        },
        setStatusPopup(state, data){
            // записывает статус popup в положение data
            state.togglePopup = data;
        },
        setDefaultValue(state){
            // записывает значения по умолчанию
            state.orderInfo = null;
            state.togglePopup = false;
        },
    },
    actions: {
        async putOrderInfo({commit}, data){
            // получает и устанавливает orderInfo - информация о заказе
            let responce = await fetch(`http://efit.biz:8080/operatororderprocessor/api/orders/${data}/info`);
            if(responce.ok){
                let result = await responce.json();
                commit('setOrderInfo', result);
            }else{
                alert("Ошибка HTTP: " + response.status);
            }
        },
        putStatusPopup({commit}, data){
            // устанавливает статус popup
            commit('setStatusPopup', data);
        },
        clearStore({commit}){
            // устанавливает дефолтные значения
            commit('setDefaultValue');
        }
    }
}